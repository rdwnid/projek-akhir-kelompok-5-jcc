<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

// Route untuk CRUD Film
Route::resource('film', 'FilmController');

// Route untuk CRUD Pemain Film
Route::resource('pemain_film', 'PemainFilmController');

// Route untuk CRUD Genre
Route::resource('genre', 'GenreController')->only(['create','store']);

// Route untuk CRUD Kewarganegaraan
Route::resource('negara', 'NegaraController')->only(['create','store']);

// Route untuk CRUD rating usia
Route::resource('rating_usia', 'RatingUsiaController')->only(['create','store']);

// Route untuk CRUD ulasan
Route::resource('ulasan', 'UlasanController')->only(['store']);

// Route untuk CRUD Peran
Route::get('/peran/{id}/create', 'PeranController@create');
Route::post('/peran', 'PeranController@store');

// Route untuk Profile
Route::resource('profile', 'ProfileController')->only([
    'index', 'update'
]);

Auth::routes();

Route::get('/home', 'HomeController@index');
