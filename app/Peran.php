<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class Peran extends Model
{
  protected $table = 'peran';
  protected $fillable = ['peran','pemain_film_id','film_id'];
  public function pemain_film() {
    return $this->belongsTo('App\PemainFilm','pemain_film_id');
  }
  public function film() {
    return $this->belongsTo('App\Film','film_id');
  }
}

?>