<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Negara;
use App\PemainFilm;

class PemainFilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $negara = Negara::all();
        return view('pemain_film.create',compact('negara'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => 'required|string|max:255',
                'tanggal_lahir' => 'required',
                'negara_id' => 'required',
                'foto' => 'mimes:jpeg,jpg,png|max:2200'
            ]
        );
        $pemain_film = new PemainFilm;
        if ($request->has('foto')) {
            $path = 'foto_profile/';
            $gambar = $request->foto;
            $new_gambar = time() . '-' . $gambar->getClientOriginalName();
            $gambar->move('foto_profile/', $new_gambar);
            $pemain_film->foto = $new_gambar;
        }
        $pemain_film->nama = $request->nama;
        $pemain_film->tanggal_lahir = $request->tanggal_lahir;
        $pemain_film->negara_id = $request->negara_id;
        $pemain_film->bio = $request->bio;
        $pemain_film->save();
        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pemain_film = PemainFilm::find($id);
        $negara = Negara::all();
        return view('pemain_film.edit', compact('pemain_film','negara'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama' => 'required|string|max:255',
                'tanggal_lahir' => 'required',
                'negara_id' => 'required',
                'foto' => 'mimes:jpeg,jpg,png|max:2200'
            ]
        );

        $pemain_filme = PemainFilm::find($id);
        if ($request->has('foto')) {
            $path = 'foto_profile/';
            if ($pemain_filme->foto != "") {
                File::delete($path . $film->poster);   
            }
            $gambar = $request->foto;
            $new_gambar = time() . '-' . $gambar->getClientOriginalName();
            $gambar->move('foto_profile/', $new_gambar);

            $pemain_filme->foto = $new_gambar;
        }
        $pemain_filme->nama = $request->nama;
        $pemain_filme->tanggal_lahir = $request->tanggal_lahir;
        $pemain_filme->negara_id = $request->negara_id;
        $pemain_filme->bio = $request->bio;
        $pemain_filme->save();
        return redirect('/film');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
