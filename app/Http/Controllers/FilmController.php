<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Genre;
use App\RatingUsia;
use App\Film;
use App\Peran;
use App\Ulasan;
use File;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $film = Film::all();
        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        $rating_usia = RatingUsia::all();
        return view('film.create',compact('genre','rating_usia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'judul' => 'required',
                'tahun' => 'required',
                'deskripsi' => 'required',
                'durasi' => 'required',
                'production' => 'required',
                'genre_id' => 'required',
                'rating_usia_id' => 'required',
                'poster' => 'required|mimes:jpeg,jpg,png|max:2200'
            ],
            [
                'judul.required' => 'Judul harus diisi!',
                'tahun.required' => 'Tahun harus diisi!',
                'deskripsi.required' => 'Deskripsi harus diisi!',
                'durasi.required' => 'Durasi harus diisi!',
                'production.required' => 'Produksi harus diisi!',
                'poster.required' => 'Poster harus diisi!',
                'poster.mimes' => 'Poster hanya support jpeg, jpg dan png',
                'genre_id' => 'Genre harus diisi',
                'rating_usia_id' => 'Rating usia harus diisi'
            ]
        );

        $gambar = $request->poster;
        $new_gambar = time() . '-' . $gambar->getClientOriginalName();

        $film = new Film;
        $film->judul = $request->judul;
        $film->tahun = $request->tahun;
        $film->deskripsi = $request->deskripsi;
        $film->durasi = $request->durasi;
        $film->production = $request->production;
        $film->poster = $new_gambar;
        $film->genre_id = $request->genre_id;
        $film->rating_usia_id = $request->rating_usia_id;
        $film->save();

        $gambar->move('poster/', $new_gambar);

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        $peran = Peran::where('film_id',$id)->get();
        $ulasan = Ulasan::where('film_id',$id)->get();
        return view('film.show', compact('film','peran','ulasan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::findOrFail($id);
        $genre = Genre::all();
        $rating_usia = RatingUsia::all();
        
        return view('film.edit',compact('film','genre','rating_usia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $request->validate(
            [
                'judul' => 'required',
                'tahun' => 'required',
                'deskripsi' => 'required',
                'durasi' => 'required',
                'production' => 'required',
                'genre_id' => 'required',
                'rating_usia_id' => 'required',
                'poster' => 'mimes:jpeg,jpg,png|max:2200'
            ],
            [
                'judul.required' => 'Judul harus diisi!',
                'tahun.required' => 'Tahun harus diisi!',
                'deskripsi.required' => 'Deskripsi harus diisi!',
                'durasi.required' => 'Durasi harus diisi!',
                'poster.mimes' => 'Poster hanya support jpeg, jpg dan png',
                'genre_id' => 'Genre harus diisi',
                'rating_usia_id' => 'Rating usia harus diisi'
            ]
        );

        $film = Film::find($id);
        if ($request->has('poster')) {
            $path = 'poster/';
            File::delete($path . $film->poster);
            $gambar = $request->poster;
            $new_gambar = time() . '-' . $gambar->getClientOriginalName();
            $gambar->move('poster/', $new_gambar);

            $film->poster = $new_gambar;
        }
        $film->judul = $request->judul;
        $film->tahun = $request->tahun;
        $film->deskripsi = $request->deskripsi;
        $film->durasi = $request->durasi;
        $film->production = $request->production;
        $film->genre_id = $request->genre_id;
        $film->rating_usia_id = $request->rating_usia_id;
        $film->save();

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $film =Film::find($id);
      $film->delete();
      return redirect('/film');
    }
}
