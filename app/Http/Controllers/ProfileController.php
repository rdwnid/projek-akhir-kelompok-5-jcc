<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profile;
use App\Negara;
use App\User;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {
        $id = Auth::id();
        $profile = Profile::where('user_id',$id)->first();
        $user = User::find($id);
        $negara = Negara::all();
        return view('profile.edit', compact('profile','negara'));
    }
    public function update(Request $request, $id) {
        $request->validate(
            [
                'nama' => 'required|string|max:255',
                'tanggal_lahir' => 'required',
                'negara_id' => 'required',
                'foto' => 'mimes:jpeg,jpg,png|max:2200'
            ]
        );

        $profile = Profile::find($id);
        if ($request->has('foto')) {
            $path = 'foto_profile/';
            if ($profile->foto != "") {
                File::delete($path . $film->poster);   
            }
            $gambar = $request->foto;
            $new_gambar = time() . '-' . $gambar->getClientOriginalName();
            $gambar->move('foto_profile/', $new_gambar);

            $profile->foto = $new_gambar;
        }
        $profile->nama = $request->nama;
        $profile->tanggal_lahir = $request->tanggal_lahir;
        $profile->negara_id = $request->negara_id;
        $profile->bio = $request->bio;
        $profile->save();


    }
}
