<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class PemainFilm extends Model
{
  protected $table = 'pemain_film';
  protected $fillable = ['nama', 'tanggal_lahir', 'foto', 'bio', 'negara_id'];
  public function negara() {
    return $this->belongsTo('App\Negara');
  }
  public function peran() {
    return $this->hasMany('App\Peran');
  }
}

?>