<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Negara extends Model
{
    protected $table = "negara";
    protected $fillable = ["nama"];
    public function profile() {
        return $this->hasMany('App\Profile');
    }
    public function pemain_film() {
        return $this->hasMany('App\PemainFilm');
    }
}
