<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class RatingUsia extends model
{
  protected $table = 'rating_usia';
  protected $fillable = ['nama'];
  public function film() {
    return $this->hasMany('App\Film');
  }
}

?>