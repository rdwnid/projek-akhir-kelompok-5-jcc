<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class Film extends Model
{
  protected $table = 'film';
  protected $fillable = [
    'judul', 'tahun', 'deskripsi', 'durasi', 'production', 'poster', 'genre_id', 'rating_usia_id'
  ];
  public function genre() {
    return $this->belongsTo('App\Genre');
  }
  public function rating_usia() {
    return $this->belongsTo('App\RatingUsia');
  }
  public function peran() {
    return $this->hasMany('App\Peran');
  }
  public function ulasan() {
    return $this->hasMany('App\Ulasan');
  }

}

?>