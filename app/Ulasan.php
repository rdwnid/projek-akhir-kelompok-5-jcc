<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class Ulasan extends Model
{
  protected $table = "ulasan";
  protected $fillable = ["komentar","rating", "user_id", "film_id"];
  public function user() {
    return $this->belongsTo('App\User','user_id');
  }
  public function film() {
    return $this->belongsTo('App\User','film_id');
  }
}

?>