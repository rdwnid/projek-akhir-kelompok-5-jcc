<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Profile extends Model
{
    protected $table="profile" ;
    protected $fillable=['nama', 'tanggal_lahir', 'foto', 'bio', 'user_id', 'negara_id'];
    public function user() {
        return $this->belongsTo('App\User');
    }
    public function negara() {
        return $this->belongsTo('App\Negara');
    }
}
