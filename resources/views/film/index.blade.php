
<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reviews Film</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/plyr.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body>
    
    @include('partial.header')
    <br>

    <a href="/film/create" class="btn btn-primary my-3">Tambah Film</a>
    <a href="/pemain_film/create" class="btn btn-primary my-3">Tambah Pemain Film</a>
    <a href="/genre/create" class="btn btn-primary my-3">Tambah Genre</a>
    <a href="/negara/create" class="btn btn-primary my-3">Tambah Negara</a>
    <a href="/rating_usia/create" class="btn btn-primary my-3">Tambah Batasan Usia</a>
    <table class="table table-bordered">
        <thead class="text-white">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Judul</th>
            <th scope="col">Tahun</th>
            <th scope="col">Deskripsi</th>
            <th scope="col">Durasi (Menit)</th>
            <th scope="col">Produksi</th>
            <th scope="col">Poster</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody class="text-white">
          @forelse ($film as $key => $item)
              <tr>
                  <td>{{$key + 1}}</td>
                  <td>{{$item->judul}}</td>
                  <td>{{$item->tahun}}</td>
                  <td>{{$item->deskripsi}}</td>
                  <td>{{$item->durasi}}</td>
                  <td>{{$item->production}}</td>
                  <td>{{$item->poster}}</td>
                  <td>
                    <form action="/film/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/film/{{$item->id}}" class="m-1 btn btn-info btn-sm">Detail</a>
                        <a href="/film/{{$item->id}}/edit" class="m-1 btn btn-warning btn-sm">Edit</a>
                        <a href="/peran/{{$item->id}}/create" class="m-1 btn btn-success btn-sm">Tambah Pemeran</a>
                        <input type="submit" class="m-1 btn btn-danger btn-sm" value="Delete">
                    </form>
                  </td>
              </tr>
          @empty
              <h1>Data Kosong</h1>
          @endforelse
        </tbody>
      </table>

<!-- Footer Section Begin -->
      <br><br><br>
    @include('partial.footer')
<!-- Footer Section End -->

<!-- Js Plugins -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/player.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/mixitup.min.js"></script>
<script src="js/jquery.slicknav.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/main.js"></script>


</body>

</html>