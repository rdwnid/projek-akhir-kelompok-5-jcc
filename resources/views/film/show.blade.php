
<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detail Film</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/plyr.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    @include('partial.header')
    <!-- Header End -->

    <br><br><br>
    <section class="anime-details spad">
        <div class="container">
            <div class="anime__details__content">
                <div class="row">
                    <div class="col-lg-3">
                        {{-- data-setbg ambil dari database @poster --}}
                        <div class="anime__details__pic set-bg" data-setbg="{{ asset('poster/' . $film->poster) }}">
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="anime__details__text">
                            <div class="anime__details__title">
                                {{-- judul h3 ambil dari databse @judul --}}
                                {{-- sisanya sama ambil dari database @tahun,@deskripsi,.... --}}
                                <h3>{{ $film->judul }}</h3>
                            </div>
                            <div class="anime__details__rating">
                                <div class="rating">
                                    <a href="#"><i class="fa fa-star"></i></a>
                                    <a href="#"><i class="fa fa-star"></i></a>
                                    <a href="#"><i class="fa fa-star"></i></a>
                                    <a href="#"><i class="fa fa-star"></i></a>
                                    <a href="#"><i class="fa fa-star"></i></a>
                                </div>
                            </div>
                            <p>{{ $film->deskripsi }}</p>
                            <div class="anime__details__widget">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <ul>
                                            <li><span>Tahun :</span> {{ $film->tahun }}</li>
                                            <li><span>Durasi :</span> {{ $film->durasi }} menit</li>
                                            <li><span>Produksi :</span> {{ $film->production }}</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <h5 class="text-white">Pemeran</h5>
                                        <p>
                                            @forelse ($peran as $item)
                                            <a href="/pemain_film/{{ $item->pemain_film_id}}"><span>{{ $item->pemain_film->nama }}</span></a>,
                                            @empty
                                            <span>Belum ada data pemain.</span>
                                            @endforelse
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>

                {{-- diambil dari database @ulasan --}}
                <br>
                <div class="row">
                    <div class="col-lg-8 col-md-8">
                        
                        <div class="anime__details__review">
                            <div class="section-title">
                                <h5>Ulasan</h5>
                            </div>
                            @forelse ($ulasan as $item)
                            <div class="anime__review__item">
                                <div class="anime__review__item__pic">
                                    <img src="{{ asset('foto_profile/' . $item->user->profile->foto) }}" alt="">
                                </div>
                                <div class="anime__review__item__text">
                                    <h6>{{ $item->user->profile->nama }}</h6>
                                    <p>{{ $item->komentar }}</p>
                                    <p>
                                        @for ($i = 0; $i < $item->rating; $i++)
                                        <i class="fa fa-star"></i>
                                        @endfor
                                    </p>
                                </div>
                            </div>
                            @empty
                            @endforelse

                        {{-- comment masuk ke database ulasan --}}

                        <div class="anime__details__form">
                            <div class="section-title">
                                <h5>Ulasan Anda</h5>
                            </div>
                            <form action="/ulasan" method="post">
                                @csrf
                                <label class="text-white">Komentar</label><br>
                                <textarea name="komentar"></textarea>
                                <label class="text-white">Rating</label><br>
                                <input type="number" min="0" max="5" name="rating" required>
                                <br><br>
                                <input type="hidden" name="film_id" value="{{ $film->id }}">
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                <button type="submit"><i class="fa fa-location-arrow"></i> Kirim</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <br><br><br>

<!-- Footer Section Begin -->
    @include('partial.footer')
<!-- Footer Section End -->

<!-- Js Plugins -->
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/player.js')}}"></script>
<script src="{{asset('js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('js/mixitup.min.js')}}"></script>
<script src="{{asset('js/jquery.slicknav.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>


</body>

</html>