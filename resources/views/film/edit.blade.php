
<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Film</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/plyr.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    @include('partial.header')
    <!-- Header End -->

    <br><br><br>
    <form action="/film/{{ $film->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-6">
                    <label class="text-white">Judul Film</label>
                    <input type="text" class="form-control" name="judul" value="{{ $film->judul }}">
                </div>
            </div>
        </div>
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-6">
                    <label class="text-white">Tahun</label>
                    <input type="number" min="1900" max="2099" step="1" class="form-control" name="tahun" value="{{ $film->tahun }}">
                </div>
            </div>
        </div>
        @error('tahun')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-6">
                    <label class="text-white">Deskripsi</label>
                    <textarea name="deskripsi" class="form-control" cols="30" rows="5">{{ $film->deskripsi }}</textarea>
                </div>
            </div>
        </div>
        @error('deskripsi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-6">
                    <label class="text-white">Durasi (Menit)</label>
                    <input type="text" class="form-control" name="durasi" value="{{ $film->durasi }}">
                </div>
            </div>
        </div>
        @error('durasi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-6">
                    <label class="text-white">Produksi</label>
                    <input type="text" class="form-control" name="production" value="{{ $film->production }}">
                </div>
            </div>
        </div>
        @error('production')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-6">
                    <label class="text-white">Poster</label>
                    <input type="file" class="form-control" name="poster">
                    @error('poster')
                        <div class="mt-2 alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="row justify-content-center align-items-center">
            <div class="col-lg-6">
                <label class="text-white">Genre</label>
            </div>
        </div>
        <div class="form-group">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-6">
                    <select id="genre_id" class="form-control" name="genre_id">
                        <option  value=""> --Pilih Genre-- </option>
                        @foreach ($genre as $item)
                        @if ( $film->genre_id == $item->id)
                            <option  value="{{ $item->id }}" selected> {{ $item->nama }} </option>
                        @else
                            <option  value="{{ $item->id }}"> {{ $item->nama }} </option>
                        @endif
            
                        @endforeach
                    </select>
                    @error('genre_id')
                        <div class="mt-2 alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="row justify-content-center align-items-center">
            <div class="col-lg-6">
                <label class="text-white">Rating Usia</label>
            </div>
        </div>
        <div class="form-group">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-6">
                    <select id="rating_usia_id" class="form-control" name="rating_usia_id">
                        <option  value=""> --Pilih Rating-- </option>
                        @foreach ($rating_usia as $item)
                        @if ( $film->rating_usia_id == $item->id)
                            <option  value="{{ $item->id }}" selected> {{ $item->nama }} </option>
                        @else
                            <option  value="{{ $item->id }}"> {{ $item->nama }} </option>
                        @endif
            
                        @endforeach
                    </select>
                    @error('rating_usia_id')
                        <div class="mt-2 alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="row justify-content-center align-items-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
      <br><br><br>

<!-- Footer Section Begin -->
    @include('partial.footer')
<!-- Footer Section End -->

<!-- Js Plugins -->
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/player.js')}}"></script>
<script src="{{asset('js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('js/mixitup.min.js')}}"></script>
<script src="{{asset('js/jquery.slicknav.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>


</body>

</html>