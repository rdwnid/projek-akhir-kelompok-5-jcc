<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <div class="header__logo">
                    <a href="./index.html">
                        <img src="{{asset('img/logo.png')}}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="header__nav">
                    <nav class="header__menu mobile-menu">
                        <ul>
                            <li class="active"><a href="/home">Homepage</a></li>
                            <li><a href="#">Genre<span class="arrow_carrot-down"></span></a>
                                <ul class="dropdown">
                                    <li><a href="./categories.html">Adventure</a></li>
                                    <li><a href="./anime-details.html">Horror</a></li>
                                    <li><a href="./anime-watching.html">Romance</a></li>
                                    <li><a href="./blog-details.html">Animation</a></li>
                                </ul>
                            </li>
                            <li><a href="/film">List Film</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="header__right">
                    <a href="#" class="search-switch"><span class="icon_search"></span></a>
                    @guest
                            <a class="" href="{{ route('login') }}">{{ __('Login') }}</a>
                        @if (Route::has('register'))
                                <a class="" href="{{ route('register') }}">{{ __('Register') }}</a>
                        @endif
                    @else
                                <a class="" href="/profile">{{ Auth::user()->profile->nama }}</a>
                                <a class="" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                    @endguest
                </div>
            </div>
        </div>
        <div id="mobile-menu-wrap"></div>
    </div>
</header>