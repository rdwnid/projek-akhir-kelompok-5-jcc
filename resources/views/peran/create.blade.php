
<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tambah Pemeran Film</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/plyr.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    @include('partial.header')
    <!-- Header End -->

    <br><br><br>
    <form method="POST" action="/peran">
        @csrf

        <div class="form-group row">
            <label for="peran" class="text-white col-md-4 col-form-label text-md-right">{{ __('Nama Peran') }}</label>
            <div class="col-md-6">
                <input id="peran" type="text" class="form-control @error('peran') is-invalid @enderror" name="peran" value="{{ old('peran') }}" required autocomplete="peran" autofocus>
                @error('peran')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>  

        <input type="hidden" value={{ $film_id }} name="film_id">

        <div class="form-group row">
            <label for="pemain_film_id" class="text-white col-md-4 col-form-label text-md-right">{{ __('Aktor') }}</label>

            <div class="col-md-6">
                <select id="pemain_film_id" class="form-control @error('pemain_film_id') is-invalid @enderror" name="pemain_film_id" autofocus>
                    <option  value=""> --Pilih Aktor-- </option>
                    @foreach ($pemain_film as $item)
                    @if (old('pemain_film_id') == $item->id)
                        <option  value="{{ $item->id }}" selected> {{ $item->nama }} </option>
                    @else
                        <option  value="{{ $item->id }}"> {{ $item->nama }} </option>
                    @endif
        
                    @endforeach
                </select>
                @error('pemain_film_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Tambah Pemain') }}
                </button>
            </div>
        </div>
    </form>
    <br><br><br>

<!-- Footer Section Begin -->
    @include('partial.footer')
<!-- Footer Section End -->

<!-- Js Plugins -->
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/player.js')}}"></script>
<script src="{{asset('js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('js/mixitup.min.js')}}"></script>
<script src="{{asset('js/jquery.slicknav.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>


</body>

</html>