<img src="https://gitlab.com/rdwnid/projek-akhir-kelompok-5-jcc/-/blob/main/public/erd.png" width="400">

## Kelompok 5

- Muhamad Ridwan
- Andri Taufiq
- Fajrin Riyanto

## Tema

Website Review Film seperti IMDB tetapi lebih sederhana

## Link Video

Penjelasan detail projek bisa diakses [disini.](https://drive.google.com/drive/folders/1xA26zbhXtlsawfqIkXn8IBO5oalo3HGI?usp=sharing)

## Link Demo

Demo web dapat diakses [disini](https://polar-island-98267.herokuapp.com/)
